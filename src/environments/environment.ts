// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig :{
    apiKey: "AIzaSyBVUQ7uK-0Q5R72QuEU2k4AbZukbaVgqxw",
    authDomain: "testb-24d49.firebaseapp.com",
    databaseURL: "https://testb-24d49.firebaseio.com",
    projectId: "testb-24d49",
    storageBucket: "testb-24d49.appspot.com",
    messagingSenderId: "884601578344",
    appId: "1:884601578344:web:47ce5ae5ab431f7d282cdd",
    measurementId: "G-LP60MX1TNN"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
