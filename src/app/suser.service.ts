import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Userapi } from './interfaces/userapi';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import 'firebase/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SuserService {

  apiuser = "https://jsonplaceholder.typicode.com/users"
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  docCollection:AngularFirestoreCollection;

  getusers():Observable<any>{
    return this.http.get<Userapi[]>(this.apiuser);
  }

  addusertodb(userId:string,uid:number, email:string){
    const usr = {uid:uid, email:email};
    this.userCollection.doc(userId).collection('alluser').add(usr);
  }



  constructor(private http: HttpClient,private db:AngularFirestore) { }
  getusr(userId):Observable<any[]>{
    this.docCollection = this.db.collection(`users/${userId}/alluser`);
        console.log('alluser collection created');
        return this.docCollection.snapshotChanges().pipe(
          map(actions => actions.map(a => {
            const data = a.payload.doc.data();
            data.id = a.payload.doc.id;
            return { ...data };
          }))
        ); 
  }

}
