export interface Post {
    userId: number;
    id: number;
    username: string;
    email: string;
}

